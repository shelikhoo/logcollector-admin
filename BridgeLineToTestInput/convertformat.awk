function get_hashedFingerprint_from_fingerprint(fingerprint) {
  cmd = "xxd -r -p |openssl dgst -sha1 --hex"
  print fingerprint |& cmd
  close(cmd, "to")
  cmd |& getline line
  close(cmd)

  split(line, lineOut, " ")
  hashed_fingerprint=toupper(lineOut[2])

  return hashed_fingerprint
}
{
 if ($0!="") {
   bridge_type=$2
   bridge_address=$3
   bridge_fingerprint=$4
   bridge_name=get_hashedFingerprint_from_fingerprint(bridge_fingerprint)
   bridge_detail2=$5
   bridge_detail3=$6

   print(bridge_name "," bridge_type, bridge_address, bridge_fingerprint, bridge_detail2, bridge_detail3)
 }
}
