#!/usr/bin/env bash
#git clone https://@github.com/ShellBot-BridgeAccessibilityAutomation/BridgeStatus.git
#ln /var/lib/torprobed/jsonlog.jsonl /var/lib/torprobed-publish-result/jsonlog.jsonl
#HOME
cat $REPORT_FILE|jq -r '[.testName, .detail.bootstrapPercentage, .clientReportGenerationTime, .testSite, (.clientReportGenerationTime|tonumber|todateiso8601), .testLine]|@tsv'|gawk -f $REPORT_SCRIPT -

for file in recentResult_*
do
 sort -o $file $file
done

git config --global user.name "ShellBot-BridgeAccessibilityAutomation"
git config --global user.email "shellbot+ujyg2iip7gkc60ibss0d09nmi9wclusriszz@unial.org"

git add -A
git commit -m "update"
git push origin
