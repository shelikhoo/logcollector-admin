BEGIN{
  FS="\t"
  OFS="\t"
  MostRecentTest=0
}
function ensureExist(name, testsite) {
    if ( !(isarray(known_result) && testsite in known_result) || !(isarray(known_result[testsite]) && name in known_result[testsite]) ){
      #print name, testsite
      if(!isarray(known_result[testsite])){
        delete known_result[testsite]
      }
      known_result[testsite][name]["exists"]=1
      known_result[testsite][name]["clientReportGenerationTime"]=0
    }
    return
}
{
  testName = $1
  bootstrapPercentage = $2
  clientReportGenerationTime = $3
  testSite = $4
  clientReportGenerationTimeAsString = $5
  testLine = $6
  if (bootstrapPercentage!="" && testSite != "" && testName!=""){
    ensureExist(testName, testSite)
    if(known_result[testSite][testName]["clientReportGenerationTime"]<clientReportGenerationTime){
      known_result[testSite][testName]["clientReportGenerationTime"]=clientReportGenerationTime
      known_result[testSite][testName]["clientReportGenerationTimeAsString"]=clientReportGenerationTimeAsString
      known_result[testSite][testName]["bootstrapPercentage"]=bootstrapPercentage
      known_result[testSite][testName]["testLine"]=testLine
    }
    if (MostRecentTest<clientReportGenerationTime) {
      MostRecentTest = clientReportGenerationTime
    }
  }
}
END{
  for (testSite_i in known_result) {
      for(testName_i in known_result[testSite_i]){

        if(known_result[testSite_i][testName_i]["clientReportGenerationTime"]<MostRecentTest - 86400) {
          continue
        }

        cmd = "openssl dgst -sha3-512 -hmac "ENVIRON["IP_MASKING_HAMCKEY"]" --hex"
        print known_result[testSite_i][testName_i]["testLine"] |& cmd
        close(cmd, "to")
        cmd |& getline line
        close(cmd)

        split(line, lineOut, " ")
        bridgeLineMaskedHash=lineOut[2]
        print testName_i, known_result[testSite_i][testName_i]["bootstrapPercentage"], known_result[testSite_i][testName_i]["clientReportGenerationTimeAsString"], known_result[testSite_i][testName_i]["clientReportGenerationTime"], bridgeLineMaskedHash > "recentResult_" testSite_i
      }
  }
}
