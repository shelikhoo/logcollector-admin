#!/usr/bin/env bash
cd /tmp
git clone https://...@github.com/ShellBot-BridgeAccessibilityAutomation/BridgeLines.git bridgelines
cd bridgelines
git push https://...@gitlab.torproject.org/tpo/anti-censorship/connectivity-measurement/bridgelines.git HEAD:main
