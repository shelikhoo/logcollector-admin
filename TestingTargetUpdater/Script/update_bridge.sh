#!/usr/bin/env bash

pushd BridgeLines
git pull
cat bridgelines.json | jq -r .bridgelines[] > $DATADIR/dynamic_bridges
cat frontdesk.json | jq -r .bridgelines[] >> $DATADIR/dynamic_bridges
popd
pushd $DATADIR
gawk -f convertformat.awk dynamic_bridges > dynamic_bridges.testline
cat dynamic_bridges.testline static.testline > testing_objective.testline
popd
